/// <reference path="../Definitions/knockout.d.ts" />
/// <reference path="../Definitions/jquery.d.ts" />
var Rsl;
(function (Rsl) {
    var ApplicationViewModel = (function () {
        // get applicant to add a loader here
        function ApplicationViewModel(_apiAccess) {
            var _this = this;
            this._apiAccess = _apiAccess;
            this.streetlights = ko.observable();
            this.selectedStreetlight = ko.observable();
            this.selectedStreetbulb = ko.observable();
            this.loadData().done(function (x) {
                _this.streetlights(x);
            });
            this.computedPowerDrawn = ko.observable();
            this.loadingCss = ko.observable(false);
            this.isBulbFaulty = ko.observable(false);
            this.hideMain = ko.observable(false);
        }
        ApplicationViewModel.prototype.close = function () {
            this.isBulbFaulty(false);
            this.hideMain(false);
        };
        //loads selection into selected street light
        ApplicationViewModel.prototype.selectStreetlight = function (parent, streetlight) {
            parent.selectedStreetlight(null);
            parent.loadDetails(streetlight.id).done(function (x) {
                parent.selectedStreetlight(x);
                parent.computePowerDrawn(x);
                /**
                 Fix below to update view when promise is returned. Would have been easy if we used KO 3.4
                 */
                ko.cleanNode(document.getElementById("bulber"));
                ko.applyBindings(parent, document.getElementById("bulber"));
            });
        };
        //loads selection into selected street bulb
        ApplicationViewModel.prototype.selectStreetbulb = function (parent, bulb) {
            parent.selectedStreetbulb(null);
            parent.loadDetails(bulb.bulbInformation.id).done(function (x) {
                parent.selectedStreetbulb(x);
            });
        };
        ApplicationViewModel.prototype.clickObject = function (parent, data) {
            parent.set(data);
        };
        ApplicationViewModel.prototype.isFailed = function (bulb) {
            return bulb.bulbStatus().fault > 0;
        };
        ApplicationViewModel.prototype.toggleLightState = function (light) {
            var _this = this;
            var isOn = light.isSwitchedOn();
            this.loadingCss(false);
            if (isOn) {
                this.loadingCss(true);
                this._apiAccess.switchOffLight(light.id).always(function (x) {
                    _this.selectStreetlight(_this, {
                        id: light.id,
                        description: light.description
                    });
                    _this.loadingCss(false);
                });
                this.computedPowerDrawn = 0;
            }
            else {
                this.loadingCss(true);
                this._apiAccess.switchOnLight(light.id).always(function (x) {
                    _this.selectStreetlight(_this, {
                        id: light.id,
                        description: light.description
                    });
                    _this.loadingCss(false);
                });
            }
        };
        ApplicationViewModel.prototype.toggleBulbState = function (thisHandle, bulb) {
            var isOn = bulb.bulbStatus().isOn;
            thisHandle.isBulbFaulty(false);
            thisHandle.hideMain(false);
            var faultStatus = bulb.bulbStatus().faultCondition;
            if (faultStatus == null || faultStatus == 0) {
                if (isOn) {
                    // always switch off
                    thisHandle._apiAccess.switchOffBulb(bulb.bulbInformation.id)
                        .done(function (x) {
                        // reload bulb data
                        thisHandle.updateBulbStatus(bulb);
                    });
                }
                else {
                    thisHandle._apiAccess.switchOnBulb(bulb.bulbInformation.id)
                        .done(function (x) {
                        // reload bulb data
                        thisHandle.updateBulbStatus(bulb);
                    });
                }
            }
            else {
                thisHandle.isBulbFaulty(true);
                thisHandle.hideMain(true);
            }
        };
        ApplicationViewModel.prototype.updateBulbFaultStatus = function (thisHandle, bulb) {
            var faultStatus = bulb.bulbStatus().faultCondition;
            if (faultStatus == null || faultStatus == 0) {
                //set bulb to have a general fault
                thisHandle._apiAccess.registerFault(bulb.bulbInformation.id, "1").done(function (x) {
                    bulb.bulbStatus().faultCondition = 1;
                    //                    thisHandle.isBulbFaulty(true);
                    thisHandle.updateBulbStatus(bulb);
                });
            }
            else {
                //set bulb to have a general fault
                thisHandle._apiAccess.registerFault(bulb.bulbInformation.id, "0").done(function (x) {
                    bulb.bulbStatus().faultCondition = 0;
                    //                    thisHandle.isBulbFaulty(false);
                    thisHandle.updateBulbStatus(bulb);
                });
            }
        };
        ApplicationViewModel.prototype.updateBulbStatus = function (bulb) {
            var _this = this;
            this._apiAccess.loadBulbDetail(bulb.bulbInformation.id).done(function (x) {
                bulb.bulbStatus(x.bulbCurrentState);
                _this.computePowerDrawn(_this.selectedStreetlight());
                /**
Fix below to update view when promise is returned. Would have been easy if we used KO 3.4
*/
                ko.cleanNode(document.getElementById("bulber"));
                ko.applyBindings(_this, document.getElementById("bulber"));
            });
        };
        ApplicationViewModel.prototype.loadData = function () {
            return this._apiAccess.loadStreetlights();
        };
        ApplicationViewModel.prototype.loadDetails = function (id) {
            return this._apiAccess.loadStreetlightDetail(id);
        };
        ApplicationViewModel.prototype.computePowerDrawn = function (light) {
            var streetLightPower = light.electricalDraw;
            var bulbComputedPower = 0;
            for (var index in light.bulbs) {
                var bulb = light.bulbs[index];
                if (bulb.bulbStatus().isOn) {
                    bulbComputedPower = bulbComputedPower + bulb.bulbInformation.powerDraw;
                }
                else {
                    streetLightPower = 0;
                }
            }
            this.computedPowerDrawn = (streetLightPower + bulbComputedPower);
        };
        ApplicationViewModel.prototype.updateComputedPowerDrawn = function (bulb) {
            var current = this.computedPowerDrawn;
            var computed = 0;
            if (bulb.bulbStatus().isOn) {
                computed = (current + bulb.bulbInformation.powerDraw);
            }
            else {
                computed = (current - bulb.bulbInformation.powerDraw);
            }
            if (computed >= 0) {
                this.computedPowerDrawn = computed;
            }
            else
                this.computedPowerDrawn = 0;
        };
        return ApplicationViewModel;
    })();
    Rsl.ApplicationViewModel = ApplicationViewModel;
})(Rsl || (Rsl = {}));
