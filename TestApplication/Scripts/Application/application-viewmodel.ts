﻿/// <reference path="../Definitions/knockout.d.ts" />
/// <reference path="../Definitions/jquery.d.ts" />

module Rsl {
    export class ApplicationViewModel {
        public streetlights: KnockoutObservable<Models.IStreetlightSummary[]>;
        public selectedStreetlight: KnockoutObservable<IStreetlightDetailViewModel>;
        public selectedStreetbulb: KnockoutObservable<IBulbStateViewModel>;
        public computedPowerDrawn: KnockoutObservable<number>;
        public loadingCss: KnockoutObservable<boolean>;
        public isBulbFaulty: KnockoutObservable<boolean>;
        public hideMain: KnockoutObservable<boolean>;
        // get applicant to add a loader here
        constructor(private _apiAccess: IApiAccess) {
            

            this.streetlights = ko.observable<Models.IStreetlightSummary[]>();
            this.selectedStreetlight = ko.observable<IStreetlightDetailViewModel>();
            this.selectedStreetbulb = ko.observable<IBulbStateViewModel>();
            this.loadData().done(x => {
                this.streetlights(x);
            });
            this.computedPowerDrawn = ko.observable<number>();
            this.loadingCss = ko.observable<boolean>(false);
            this.isBulbFaulty = ko.observable<boolean>(false);
            this.hideMain = ko.observable<boolean>(false);
         

        }

        public close(): void {
            this.isBulbFaulty(false);
            this.hideMain(false);
        }
      
        //loads selection into selected street light
        public selectStreetlight(parent: ApplicationViewModel, streetlight: Models.IStreetlightSummary): void {
            parent.selectedStreetlight(null);
            parent.loadDetails(streetlight.id).done(x => {
                parent.selectedStreetlight(x);
                parent.computePowerDrawn(x);
                /**
                 Fix below to update view when promise is returned. Would have been easy if we used KO 3.4
                 */
                ko.cleanNode(document.getElementById("bulber"));
                ko.applyBindings(parent, document.getElementById("bulber"));
                
              
            });
        }

        //loads selection into selected street bulb
        public selectStreetbulb(parent: ApplicationViewModel, bulb: IBulbStateViewModel): void {
            parent.selectedStreetbulb(null);
            parent.loadDetails(bulb.bulbInformation.id).done(x => {
                parent.selectedStreetbulb(<any>x);
            });
        }

        public clickObject(parent:any, data: any): void {
            parent.set(data);
        }
        public isFailed(bulb: IBulbStateViewModel): boolean {
            return bulb.bulbStatus().fault > 0;
        }

        public toggleLightState(light: IStreetlightDetailViewModel): void {
            var isOn = light.isSwitchedOn();
            this.loadingCss(false);

            if (isOn) {
                this.loadingCss(true);

                this._apiAccess.switchOffLight(light.id).always(x => {
                    this.selectStreetlight(this, {
                        id: light.id,
                        description: light.description
                    });
                    this.loadingCss(false);

                });

                this.computedPowerDrawn = <any>0;

            }
            else {
                this.loadingCss(true);

                this._apiAccess.switchOnLight(light.id).always(x => {
                    this.selectStreetlight(this, {
                        id: light.id,
                        description: light.description
                    });
                    this.loadingCss(false);

                });


                
            }


        }

        public toggleBulbState(thisHandle: any, bulb: IBulbStateViewModel): void {
            var isOn = bulb.bulbStatus().isOn;
            thisHandle.isBulbFaulty(false);
            thisHandle.hideMain(false);
            var faultStatus = bulb.bulbStatus().faultCondition;
            if (faultStatus == null || faultStatus==0) {
                if (isOn) {
                    // always switch off
                    thisHandle._apiAccess.switchOffBulb(bulb.bulbInformation.id)
                        .done(x => {
                            // reload bulb data
                            thisHandle.updateBulbStatus(bulb);


                        });
                } else {
                    thisHandle._apiAccess.switchOnBulb(bulb.bulbInformation.id)
                        .done(x => {
                            // reload bulb data
                            thisHandle.updateBulbStatus(bulb);

                        });

                }

            } else {
                thisHandle.isBulbFaulty(true);
                thisHandle.hideMain(true);

            }

            

        }
        public updateBulbFaultStatus(thisHandle: any, bulb: IBulbStateViewModel): void {


            var faultStatus = bulb.bulbStatus().faultCondition;
            if (faultStatus == null || faultStatus==0) {
                //set bulb to have a general fault
                thisHandle._apiAccess.registerFault(bulb.bulbInformation.id, "1").done(x => {
                    bulb.bulbStatus().faultCondition = 1;
//                    thisHandle.isBulbFaulty(true);
                    thisHandle.updateBulbStatus(bulb);
                });

            } else {
                //set bulb to have a general fault
                thisHandle._apiAccess.registerFault(bulb.bulbInformation.id, "0").done(x => {
                    bulb.bulbStatus().faultCondition = 0;
//                    thisHandle.isBulbFaulty(false);
                    thisHandle.updateBulbStatus(bulb);

                });


            }
        }

        private updateBulbStatus(bulb: IBulbStateViewModel) {
            this._apiAccess.loadBulbDetail(bulb.bulbInformation.id).done(x => {
                bulb.bulbStatus(x.bulbCurrentState);
                this.computePowerDrawn(this.selectedStreetlight());

                /**
Fix below to update view when promise is returned. Would have been easy if we used KO 3.4
*/
                ko.cleanNode(document.getElementById("bulber"));
                ko.applyBindings(this, document.getElementById("bulber"));
            });
        }

        private loadData(): JQueryPromise<Models.IStreetlightSummary[]> {
            return this._apiAccess.loadStreetlights();
        }

        private loadDetails(id: string): JQueryPromise<IStreetlightDetailViewModel> {
            return this._apiAccess.loadStreetlightDetail(id);
        }

       
        private computePowerDrawn(light?: IStreetlightDetailViewModel) {
            var streetLightPower = light.electricalDraw;
            var bulbComputedPower = 0;
            for (var index in light.bulbs) {
                var bulb: IBulbStateViewModel = light.bulbs[index];
                if (bulb.bulbStatus().isOn) {
                    bulbComputedPower = bulbComputedPower + bulb.bulbInformation.powerDraw;
                } else {
                    streetLightPower = 0;
                }
            }

            this.computedPowerDrawn= <any>(streetLightPower + bulbComputedPower);
        }

        private updateComputedPowerDrawn(bulb?: IBulbStateViewModel) {
            var current = this.computedPowerDrawn;
            var computed = 0;
            if (bulb.bulbStatus().isOn) {
                 computed = <any>(<any>current + bulb.bulbInformation.powerDraw);
            }
            else {
                  computed = <any>(<any>current - bulb.bulbInformation.powerDraw);
            }

            if (computed >= 0) {
                  this.computedPowerDrawn = computed;
            }
            else
                this.computedPowerDrawn = 0;
        }
    }
}